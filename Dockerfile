FROM alpine:3.14 AS base
# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
FROM base AS build
# Install Flask configparser psycopg2 
RUN apk add --no-cache --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    && pip3 install --no-cache-dir Flask configparser psycopg2 \
    && apk del --no-cache .build-deps
RUN apk add --no-cache postgresql-libs libc-dev
FROM build AS final
# Create app
RUN mkdir -p /srv/app/conf
VOLUME /srv/app
COPY web.py /srv/app
COPY web.conf /srv/app/conf
WORKDIR /srv/app
CMD python3 web.py
